package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the supplier database table.
 * 
 */
@Entity
@NamedQuery(name = "Supplier.findAll", query = "SELECT s FROM Supplier s")
public class Supplier implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String company;

	private String name;

	// bi-directional many-to-one association to SupplierHasAbstractbottle
	@OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SupplierHasAbstractbottle> supplierHasAbstractbottles;

	// bi-directional many-to-one association to SupplierHasCustomer
	@OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SupplierHasCustomer> supplierHasCustomers;

	public Supplier() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SupplierHasAbstractbottle> getSupplierHasAbstractbottles() {
		return this.supplierHasAbstractbottles;
	}

	public void setSupplierHasAbstractbottles(List<SupplierHasAbstractbottle> supplierHasAbstractbottles) {
		this.supplierHasAbstractbottles = supplierHasAbstractbottles;
	}

	public SupplierHasAbstractbottle addSupplierHasAbstractbottle(SupplierHasAbstractbottle supplierHasAbstractbottle) {
		getSupplierHasAbstractbottles().add(supplierHasAbstractbottle);
		if (supplierHasAbstractbottle.getSupplier() == null) {
			System.out.println("entro");
			supplierHasAbstractbottle.setSupplier(this);
		}
		return supplierHasAbstractbottle;
	}

	public SupplierHasAbstractbottle removeSupplierHasAbstractbottle(
			SupplierHasAbstractbottle supplierHasAbstractbottle) {
		getSupplierHasAbstractbottles().remove(supplierHasAbstractbottle);
		supplierHasAbstractbottle.setSupplier(null);

		return supplierHasAbstractbottle;
	}

	public List<SupplierHasCustomer> getSupplierHasCustomers() {
		return this.supplierHasCustomers;
	}

	public void setSupplierHasCustomers(List<SupplierHasCustomer> supplierHasCustomers) {
		this.supplierHasCustomers = supplierHasCustomers;
	}

	public SupplierHasCustomer addSupplierHasCustomer(SupplierHasCustomer supplierHasCustomer) {
		getSupplierHasCustomers().add(supplierHasCustomer);
		if (supplierHasCustomer.getSupplier() == null) {
			supplierHasCustomer.setSupplier(this);
		}
		return supplierHasCustomer;
	}

	public SupplierHasCustomer removeSupplierHasCustomer(SupplierHasCustomer supplierHasCustomer) {
		getSupplierHasCustomers().remove(supplierHasCustomer);
		supplierHasCustomer.setSupplier(null);
		return supplierHasCustomer;
	}

}