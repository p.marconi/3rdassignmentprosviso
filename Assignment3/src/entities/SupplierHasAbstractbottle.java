package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.sun.istack.internal.NotNull;

/**
 * The persistent class for the supplier_has_abstractbottle database table.
 * 
 */
@Entity
@Table(name = "supplier_has_abstractbottle")
@NamedQuery(name = "SupplierHasAbstractbottle.findAll", query = "SELECT s FROM SupplierHasAbstractbottle s")
public class SupplierHasAbstractbottle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int available;

	// bi-directional many-to-one association to Abstractbottle
	@ManyToOne
	@NotNull
	private Abstractbottle abstractbottle;

	// bi-directional many-to-one association to Supplier
	@ManyToOne
	@NotNull
	private Supplier supplier;

	public SupplierHasAbstractbottle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAvailable() {
		return this.available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public Abstractbottle getAbstractbottle() {
		return this.abstractbottle;
	}

	public void setAbstractbottle(Abstractbottle abstractbottle) {
		this.abstractbottle = abstractbottle;
		if (abstractbottle.getSupplierHasAbstractbottles() == null) {
			List<SupplierHasAbstractbottle> supp_has_bottle = new ArrayList<SupplierHasAbstractbottle>();
			supp_has_bottle.add(this);
			abstractbottle.setSupplierHasAbstractbottles(supp_has_bottle);
		} else {
			abstractbottle.addSupplierHasAbstractbottle(this);
		}
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
		if (supplier.getSupplierHasAbstractbottles() == null) {
			List<SupplierHasAbstractbottle> supp_has_bottle = new ArrayList<SupplierHasAbstractbottle>();
			supp_has_bottle.add(this);
			supplier.setSupplierHasAbstractbottles(supp_has_bottle);
		} else {
			supplier.addSupplierHasAbstractbottle(this);
		}
	}

}