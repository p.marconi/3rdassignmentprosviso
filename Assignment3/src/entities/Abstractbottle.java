package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.sun.istack.internal.NotNull;

import java.util.List;

/**
 * The persistent class for the abstractbottle database table.
 * 
 */
@Entity
@NamedQuery(name = "Abstractbottle.findAll", query = "SELECT a FROM Abstractbottle a")
public class Abstractbottle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private double alc;

	private int capacity;

	private String name;

	@NotNull
	private String type;

	// bi-directional many-to-one association to Bottle
	@OneToMany(mappedBy = "abstractbottle", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Bottle> bottles;

	// bi-directional many-to-one association to SupplierHasAbstractbottle
	@OneToMany(mappedBy = "abstractbottle")
	private List<SupplierHasAbstractbottle> supplierHasAbstractbottles;

	public Abstractbottle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAlc() {
		return this.alc;
	}

	public void setAlc(double alc) {
		this.alc = alc;
	}

	public int getCapacity() {
		return this.capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Bottle> getBottles() {
		return this.bottles;
	}

	public void setBottles(List<Bottle> bottles) {
		this.bottles = bottles;
	}

	public Bottle addBottle(Bottle bottle) {
		getBottles().add(bottle);
		bottle.setAbstractbottle(this);

		return bottle;
	}

	public Bottle removeBottle(Bottle bottle) {
		getBottles().remove(bottle);
		bottle.setAbstractbottle(null);

		return bottle;
	}

	public List<SupplierHasAbstractbottle> getSupplierHasAbstractbottles() {
		return this.supplierHasAbstractbottles;
	}

	public void setSupplierHasAbstractbottles(List<SupplierHasAbstractbottle> supplierHasAbstractbottles) {
		this.supplierHasAbstractbottles = supplierHasAbstractbottles;
	}

	public SupplierHasAbstractbottle addSupplierHasAbstractbottle(SupplierHasAbstractbottle supplierHasAbstractbottle) {
		getSupplierHasAbstractbottles().add(supplierHasAbstractbottle);
		if (supplierHasAbstractbottle.getAbstractbottle() == null) {
			supplierHasAbstractbottle.setAbstractbottle(this);
		}
		return supplierHasAbstractbottle;
	}

	public SupplierHasAbstractbottle removeSupplierHasAbstractbottle(
			SupplierHasAbstractbottle supplierHasAbstractbottle) {
		getSupplierHasAbstractbottles().remove(supplierHasAbstractbottle);
		supplierHasAbstractbottle.setAbstractbottle(null);
		return supplierHasAbstractbottle;
	}

}