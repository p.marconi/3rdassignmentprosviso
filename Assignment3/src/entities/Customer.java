package entities;

import java.io.Serializable;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the customer database table.
 * 
 */
@Entity
@NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String name;

	// bi-directional many-to-one association to Bottle
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Bottle> bottles;

	// bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name = "Custom_supply")
	private Customer customer;

	// bi-directional many-to-one association to Customer
	@OneToMany(mappedBy = "customer", cascade = CascadeType.MERGE)
	private List<Customer> customers;

	// bi-directional many-to-one association to SupplierHasCustomer
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SupplierHasCustomer> supplierHasCustomers;

	@PreRemove
	private void preRemove() {
		List<Customer> tonull = this.getCustomers();
		for (int i = 0; i < tonull.size(); i++) {
			Customer no_longer_supplied = tonull.get(i);
			if (no_longer_supplied.getId() != this.getId()) {
				tonull.get(i).setCustomer(null);
			}
		}
	}

	public Customer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Bottle> getBottles() {
		return this.bottles;
	}

	public void setBottles(List<Bottle> bottles) {
		this.bottles = bottles;
	}

	public Bottle addBottle(Bottle bottle) {
		getBottles().add(bottle);
		bottle.setCustomer(this);

		return bottle;
	}

	public Bottle removeBottle(Bottle bottle) {
		getBottles().remove(bottle);
		bottle.setCustomer(null);

		return bottle;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer_supplier) {
		this.customer = customer_supplier;
		if (customer_supplier != null) {
			if (customer_supplier.getCustomers() == null) {
				List<Customer> custom = new ArrayList<Customer>();
				custom.add(this);
				customer_supplier.setCustomers(custom);
			} else {
				customer_supplier.addCustomer(this);
			}
		}
	}

	public List<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public Customer addCustomer(Customer customer) {
		getCustomers().add(customer);
		if (customer.getCustomer() == null) {
			customer.setCustomer(this);
		}
		return customer;
	}

	public Customer removeCustomer(Customer customer) {
		getCustomers().remove(customer);
		customer.setCustomer(null);
		return customer;
	}

	public List<SupplierHasCustomer> getSupplierHasCustomers() {
		return this.supplierHasCustomers;
	}

	public void setSupplierHasCustomers(List<SupplierHasCustomer> supplierHasCustomers) {
		this.supplierHasCustomers = supplierHasCustomers;
	}

	public SupplierHasCustomer addSupplierHasCustomer(SupplierHasCustomer supplierHasCustomer) {
		getSupplierHasCustomers().add(supplierHasCustomer);
		if (supplierHasCustomer.getCustomer() == null) {
		supplierHasCustomer.setCustomer(this);
		}
		return supplierHasCustomer;
	}

	public SupplierHasCustomer removeSupplierHasCustomer(SupplierHasCustomer supplierHasCustomer) {
		getSupplierHasCustomers().remove(supplierHasCustomer);
		supplierHasCustomer.setCustomer(null);
		return supplierHasCustomer;
	}

}