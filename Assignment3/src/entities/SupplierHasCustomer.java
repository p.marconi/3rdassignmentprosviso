package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.sun.istack.internal.NotNull;


/**
 * The persistent class for the supplier_has_customer database table.
 * 
 */
@Entity
@Table(name="supplier_has_customer")
@NamedQuery(name="SupplierHasCustomer.findAll", query="SELECT s FROM SupplierHasCustomer s")
public class SupplierHasCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private double lastorderamount;

	//bi-directional many-to-one association to Customer
	@ManyToOne
	@NotNull
	private Customer customer;

	//bi-directional many-to-one association to Supplier
	@ManyToOne
	@NotNull
	private Supplier supplier;

	public SupplierHasCustomer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLastorderamount() {
		return this.lastorderamount;
	}

	public void setLastorderamount(double lastorderamount) {
		this.lastorderamount = lastorderamount;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
		if(customer.getSupplierHasCustomers() == null) {
		List<SupplierHasCustomer> supp_has_custom = new ArrayList<SupplierHasCustomer>();
		supp_has_custom.add(this);
		customer.setSupplierHasCustomers(supp_has_custom);
		}
		else {
			customer.addSupplierHasCustomer(this);
		}
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
		if(supplier.getSupplierHasCustomers() == null) {
		List<SupplierHasCustomer> supp_has_custom = new ArrayList<SupplierHasCustomer>();
		supp_has_custom.add(this);
		supplier.setSupplierHasCustomers(supp_has_custom);
		}
		else {
			supplier.addSupplierHasCustomer(this);
		}
	}

}