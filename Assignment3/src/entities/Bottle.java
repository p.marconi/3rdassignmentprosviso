package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.sun.istack.internal.NotNull;


/**
 * The persistent class for the bottle database table.
 * 
 */
@Entity
@NamedQuery(name="Bottle.findAll", query="SELECT b FROM Bottle b")
public class Bottle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@NotNull
	private int quantity;

	//bi-directional many-to-one association to Abstractbottle
	@ManyToOne
	@NotNull
	@JoinColumn(name="BottleInfo")
	private Abstractbottle abstractbottle;

	//bi-directional many-to-one association to Customer
	@ManyToOne
	@NotNull
	private Customer customer;

	public Bottle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Abstractbottle getAbstractbottle() {
		return this.abstractbottle;
	}

	public void setAbstractbottle(Abstractbottle abstractbottle) {
		this.abstractbottle = abstractbottle;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}