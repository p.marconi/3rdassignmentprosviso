package testRelationship;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class AbstractB_Bottle {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Abstractbottle Create() {
		// Create Abstract bottle
		Abstractbottle abs_bottle = new Abstractbottle();
		abs_bottle.setAlc(41.0);
		abs_bottle.setCapacity(750);
		abs_bottle.setName("Nuestra Soledad");
		abs_bottle.setType("Mezcal");
		abs_bottle.setId(4);
		//Set temp customer variable
		Customer customer = new Customer();
		customer.setId(1);
		// Create 2 different bottle, image of the same abstract bottle
		Bottle bottle1 = new Bottle();
		bottle1.setId(2);
		bottle1.setQuantity(4);
		bottle1.setAbstractbottle(abs_bottle);
		bottle1.setCustomer(customer);
		Bottle bottle2 = new Bottle();
		bottle2.setId(3);
		bottle2.setQuantity(10);
		bottle2.setAbstractbottle(abs_bottle);
		bottle2.setCustomer(customer);
		//Initialize Relationship
		List<Bottle> bottles = new ArrayList<Bottle>();
		bottles.add(bottle1);
		bottles.add(bottle2);
		abs_bottle.setBottles(bottles);
		// Add to DB
		em.getTransaction().begin();
		em.persist(customer);
		em.persist(abs_bottle);
		em.persist(bottle1);
		em.persist(bottle2);
		em.getTransaction().commit();
		abs_bottle = em.find(Abstractbottle.class, 4);
		return abs_bottle ;
	}
	
	public Bottle[] Delete() {
		// Find Entity
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 4);
		em.getTransaction().begin();
		em.remove(abs_bottle);
		em.getTransaction().commit();
		Bottle[] finalbottles = new Bottle[2];
		finalbottles[0] = em.find(Bottle.class, 2);
		finalbottles[1] = em.find(Bottle.class, 3);
		return finalbottles ;
	}

	@Test
	public void testCreateRead() {
		Abstractbottle abs_bottle = Create();
		Bottle bottle1 = (Bottle) abs_bottle.getBottles().get(0);
		Bottle bottle2 = (Bottle) abs_bottle.getBottles().get(1);
		assertNotNull(bottle1);
		assertNotNull(bottle2);
		assertTrue(bottle1.getAbstractbottle().getName().equalsIgnoreCase(bottle2.getAbstractbottle().getName()));
		assertTrue(bottle1.getAbstractbottle().getType().equalsIgnoreCase(bottle2.getAbstractbottle().getType()));
		assertEquals(4, bottle1.getQuantity());
		assertEquals(10, bottle2.getQuantity());
		assertEquals(bottle1.getAbstractbottle().getAlc(), bottle2.getAbstractbottle().getAlc(), 2) ;
		assertEquals(bottle1.getAbstractbottle().getCapacity(), bottle2.getAbstractbottle().getCapacity());
	}

	@Test
	public void testDeleteCascade() {
		Create();
		Bottle[] bottles = Delete();
		assertNull(bottles[0]); //Deleting the abstract bottle should delete also bottle instantiated
		assertNull(bottles[1]);
	}

	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Customer customer = em.find(Customer.class, 1);
		Bottle bottle1 = em.find(Bottle.class, 2);
		Bottle bottle2 = em.find(Bottle.class, 3);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 4);
		if (abs_bottle != null) { // don't delete after Deletion test
			em.remove(bottle1);
			em.remove(bottle2);
			em.remove(abs_bottle);
		}
		if(customer != null) {
			em.remove(customer);
		}
		em.getTransaction().commit();
	}

}
