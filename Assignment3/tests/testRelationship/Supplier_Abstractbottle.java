package testRelationship;

import static org.junit.Assert.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class Supplier_Abstractbottle {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public SupplierHasAbstractbottle Create() {
		// Create Entities
		Supplier supplier = new Supplier();
		supplier.setId(1);
		Abstractbottle abs_bottle = new Abstractbottle();
		abs_bottle.setId(2);
		abs_bottle.setType("Mezcal");
		SupplierHasAbstractbottle supp_bottle = new SupplierHasAbstractbottle();
		supp_bottle.setId(3);
		supp_bottle.setAvailable(56);
		//Initialize Relationship
		supp_bottle.setAbstractbottle(abs_bottle);
		supp_bottle.setSupplier(supplier);
		// Add to DB
		em.getTransaction().begin();
		em.persist(supplier);
		em.persist(abs_bottle);
		em.persist(supp_bottle);
		em.getTransaction().commit();
		supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		return supp_bottle ;
	}
	
	public SupplierHasAbstractbottle Delete() {
		// Find Entity
		SupplierHasAbstractbottle supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		em.getTransaction().begin();
		em.remove(supp_bottle);
		em.getTransaction().commit();
		supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		return supp_bottle ;
	}
	
	public SupplierHasAbstractbottle deleteCascade() {
		// Find Entity
		SupplierHasAbstractbottle supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		Supplier supplier = supp_bottle.getSupplier();
		Abstractbottle abs_bottle = supp_bottle.getAbstractbottle();
		em.getTransaction().begin();
		em.remove(supplier);
		em.remove(abs_bottle);
		em.getTransaction().commit();
		supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		return supp_bottle ;
	}

	@Test
	public void testCreateRead() {
		SupplierHasAbstractbottle supp_bottle = Create();
		assertNotNull(supp_bottle);
		assertNotNull(supp_bottle.getAbstractbottle());
		assertNotNull(supp_bottle.getSupplier());
		assertEquals(56, supp_bottle.getAvailable());
		assertTrue("Mezcal".equalsIgnoreCase(supp_bottle.getAbstractbottle().getType()));
	}

	@Test
	public void testDelete() {
		SupplierHasAbstractbottle supp_bottle = Create();
		Supplier supplier = supp_bottle.getSupplier();
		Abstractbottle abs_bottle = supp_bottle.getAbstractbottle();
		supp_bottle = Delete();
		assertNull(supp_bottle);
		assertNotNull(supplier); //Deleting the transaction, supplier and abs_bottle should not me deleted
		assertNotNull(abs_bottle);
	}
	
	@Test
	public void testDeleteCascade() {
		Create();
		SupplierHasAbstractbottle supp_bottle = deleteCascade() ;
		assertNull(supp_bottle);
	}


	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Supplier supplier = em.find(Supplier.class, 1);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 2);
		SupplierHasAbstractbottle supp_bottle = em.find(SupplierHasAbstractbottle.class, 3);
		if (supp_bottle != null) { // don't delete after Deletion test
			em.remove(supp_bottle);
		}
		if (supplier != null) {
			em.remove(supplier);
		}
		if (abs_bottle != null) {
			em.remove(abs_bottle);
		}
		em.getTransaction().commit();
	}

}