package testRelationship;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class Customer_Bottle {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Customer Create() {
		// Create Abstract bottle
		Customer customer = new Customer();
		customer.setName("Barrier");
		customer.setId(1);
		//Create a temp Abstractbottle
		Abstractbottle abs_bottle = new Abstractbottle();
		abs_bottle.setId(42);
		abs_bottle.setType("Mezcal");
		// Create 2 different bottle, image of the same abstract bottle
		Bottle bottle1 = new Bottle();
		bottle1.setId(2);
		bottle1.setQuantity(4);
		bottle1.setCustomer(customer);
		bottle1.setCustomer(customer);
		Bottle bottle2 = new Bottle();
		bottle2.setId(3);
		bottle2.setQuantity(10);
		bottle2.setCustomer(customer);
		bottle2.setCustomer(customer);
		bottle2.setAbstractbottle(abs_bottle);
		bottle1.setAbstractbottle(abs_bottle);
		//Initialize Relationship
		List<Bottle> bottles = new ArrayList<Bottle>();
		bottles.add(bottle1);
		bottles.add(bottle2);
		customer.setBottles(bottles);
		// Add to DB
		em.getTransaction().begin();
		em.persist(customer);
		em.persist(abs_bottle);
		em.persist(bottle1);
		em.persist(bottle2);
		em.getTransaction().commit();
		customer = em.find(Customer.class, 1);
		return customer;
	}
	
	public Bottle[] Delete() {
		// Find Entity
		Customer customer = em.find(Customer.class, 1);
		em.getTransaction().begin();
		em.remove(customer);
		em.getTransaction().commit();
		Bottle[] finalbottles = new Bottle[2];
		finalbottles[0] = em.find(Bottle.class, 2);
		finalbottles[1] = em.find(Bottle.class, 3);
		return finalbottles ;
	}

	@Test
	public void testCreateRead() {
		Customer customer = Create();
		Bottle bottle1 = (Bottle) customer.getBottles().get(0);
		Bottle bottle2 = (Bottle) customer.getBottles().get(1);
		assertNotNull(bottle1);
		assertNotNull(bottle2);
		assertTrue(bottle1.getCustomer().getName().equalsIgnoreCase(bottle2.getCustomer().getName()));
		assertTrue(bottle1.getAbstractbottle().getType().equalsIgnoreCase(bottle2.getAbstractbottle().getType()));
		assertEquals(4, bottle1.getQuantity());
		assertEquals(10, bottle2.getQuantity());
		assertTrue("Barrier".equalsIgnoreCase(customer.getName()));
	}

	@Test
	public void testDeleteCascade() {
		Create();
		Bottle[] bottles = Delete();
		assertNull(bottles[0]); //Deleting the customer should delete also bottle owned
		assertNull(bottles[1]);
	}

	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Customer customer = em.find(Customer.class, 1);
		Bottle bottle1 = em.find(Bottle.class, 2);
		Bottle bottle2 = em.find(Bottle.class, 3);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 42);
		if (customer != null) { // don't delete after Deletion test
			em.remove(customer);
			em.remove(bottle1);
			em.remove(bottle2);
		}
		if(abs_bottle != null) {
			em.remove(abs_bottle);
		}
		em.getTransaction().commit();
	}

}
