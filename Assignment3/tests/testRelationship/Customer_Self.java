package testRelationship;

import static org.junit.Assert.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Customer;

import org.junit.After;
import org.junit.Test;

public class Customer_Self {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Customer Create() {
		// Create Entities
		Customer customer1 = new Customer();
		customer1.setId(1);
		customer1.setName("Ghilardi Selezioni");
		Customer customer2 = new Customer();
		customer2.setId(2);
		customer2.setName("Barrier");
		//Initialize Relationship
		customer1.setCustomer(customer2);
		customer2.setCustomer(customer1);
		// Add to DB
		em.getTransaction().begin();
		em.persist(customer1);
		em.persist(customer2);
		em.getTransaction().commit();
		customer1 = em.find(Customer.class, 1);
		return customer1 ;
	}
	
	public Customer deleteCascade() {
		// Find Entity
		Customer customer1 = em.find(Customer.class, 1);
		em.getTransaction().begin();
		em.remove(customer1);
		em.getTransaction().commit();
		customer1 = em.find(Customer.class, 1);
		return customer1 ;
	}

	@Test
	public void testCreateRead() {
		Customer customer1 = Create();
		Customer customer2 = customer1.getCustomer();
		assertNotNull(customer1);
		assertNotNull(customer2);
		assertEquals(customer1.getCustomer(), customer2);
		assertEquals(customer2.getCustomer(), customer1);
		assertTrue(customer1.getCustomer().getName().equalsIgnoreCase(customer2.getName()));
	}

	@Test
	public void testDeleteCascade() {
		Customer customer1 = Create();
		Customer customer2 = customer1.getCustomer();
		customer1 = deleteCascade();
		assertNull(customer1);
		customer2 = em.find(Customer.class, 2);
		assertNotNull(customer2); //Deleting the customer, supplier customer should not be deleted
		assertNull(customer2.getCustomer());  // Null on Update
	}


	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Customer customer1 = em.find(Customer.class, 1);
		Customer customer2 = em.find(Customer.class, 2);
		if (customer1 != null) {	// don't delete after Deletion test
			em.remove(customer1);
		}
		if (customer2 != null) {
			em.remove(customer2);
		}
		em.getTransaction().commit();
	}

}