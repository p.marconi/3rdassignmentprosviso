package testRelationship;

import static org.junit.Assert.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class Supplier_Customer {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public SupplierHasCustomer Create() {
		// Create Entities
		Supplier supplier = new Supplier();
		supplier.setId(1);
		Customer customer = new Customer();
		customer.setId(2);
		SupplierHasCustomer supp_custom = new SupplierHasCustomer();
		supp_custom.setId(3);
		supp_custom.setLastorderamount(1234.56);
		//Initialize Relationship
		supp_custom.setCustomer(customer);
		supp_custom.setSupplier(supplier);
		// Add to DB
		em.getTransaction().begin();
		em.persist(supplier);
		em.persist(customer);
		em.persist(supp_custom);
		em.getTransaction().commit();
		supp_custom = em.find(SupplierHasCustomer.class, 3);
		return supp_custom ;
	}
	
	public SupplierHasCustomer Delete() {
		// Find Entity
		SupplierHasCustomer supp_custom = em.find(SupplierHasCustomer.class, 3);
		em.getTransaction().begin();
		em.remove(supp_custom);
		em.getTransaction().commit();
		supp_custom = em.find(SupplierHasCustomer.class, 3);
		return supp_custom ;
	}
	
	public SupplierHasCustomer deleteCascade() {
		// Find Entity
		SupplierHasCustomer supp_custom = em.find(SupplierHasCustomer.class, 3);
		Supplier supplier = supp_custom.getSupplier();
		Customer customer = supp_custom.getCustomer();
		em.getTransaction().begin();
		em.remove(supplier);
		em.remove(customer);
		em.getTransaction().commit();
		supp_custom = em.find(SupplierHasCustomer.class, 3);
		return supp_custom ;
	}

	@Test
	public void testCreateRead() {
		SupplierHasCustomer supp_custom = Create();
		assertNotNull(supp_custom);
		assertNotNull(supp_custom.getCustomer());
		assertNotNull(supp_custom.getSupplier());
		assertEquals(1234.56, supp_custom.getLastorderamount(), 2);
	}

	@Test
	public void testDelete() {
		SupplierHasCustomer supp_custom = Create();
		Supplier supplier = supp_custom.getSupplier();
		Customer customer = supp_custom.getCustomer();
		supp_custom = Delete();
		assertNull(supp_custom);
		assertNotNull(supplier); //Deleting the transaction, supplier and customer should not me deleted
		assertNotNull(customer);
	}
	
	@Test
	public void testDeleteCascade() {
		Create();
		SupplierHasCustomer supp_custom = deleteCascade() ;
		assertNull(supp_custom);
	}


	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Supplier supplier = em.find(Supplier.class, 1);
		Customer customer = em.find(Customer.class, 2);
		SupplierHasCustomer supp_custom = em.find(SupplierHasCustomer.class, 3);
		if (supp_custom != null) { // don't delete after Deletion test
			em.remove(supp_custom);
		}
		if (supplier != null) {
			em.remove(supplier);
		}
		if (customer != null) {
			em.remove(customer);
		}
		em.getTransaction().commit();
	}

}