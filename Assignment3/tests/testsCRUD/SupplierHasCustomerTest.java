package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class SupplierHasCustomerTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public SupplierHasCustomer Create() {
		//Create Secondary entities
		em.getTransaction().begin();
		Customer customer = new Customer();
		customer.setId(21);
		customer.setName("Barrier");
		Supplier supplier = new Supplier();
		supplier.setName("Compagnia dei Caraibi");
		supplier.setId(22);
		em.persist(customer);
		em.persist(supplier);
		em.getTransaction().commit();
		// Create main entity
		SupplierHasCustomer suphascustom = new SupplierHasCustomer();
		// Add to DB
		em.getTransaction().begin();
		suphascustom.setId(42);
		suphascustom.setSupplier(supplier);
		suphascustom.setCustomer(customer);
		em.persist(suphascustom);		
		em.getTransaction().commit();
		suphascustom = em.find(SupplierHasCustomer.class, 42);
		return suphascustom;
	}

	public SupplierHasCustomer Update() {
		// Find Entity
		SupplierHasCustomer suphascustom = em.find(SupplierHasCustomer.class, 42);
		Customer customer = em.find(Customer.class, 21);
		Supplier supplier = em.find(Supplier.class, 22);
		// Update
		em.getTransaction().begin();
		customer.setName("Le Iris");
		supplier.setName("Velier");
		suphascustom.setCustomer(customer);
		suphascustom.setSupplier(supplier);
		em.persist(customer);
		em.persist(supplier);
		em.getTransaction().commit();
		suphascustom = em.find(SupplierHasCustomer.class, 42);
		return suphascustom;
	}

	public SupplierHasCustomer Delete() {
		// Find Entities
		SupplierHasCustomer suphascustom = em.find(SupplierHasCustomer.class, 42);
		Supplier supplier = em.find(Supplier.class, 22);
		Customer customer = em.find(Customer.class, 21);
		em.getTransaction().begin();
		em.remove(suphascustom);
		em.remove(supplier);
		em.remove(customer);
		em.getTransaction().commit();
		suphascustom = em.find(SupplierHasCustomer.class, 42);
		return suphascustom;
	}

	@Test
	public void testCreateRead() {
		SupplierHasCustomer suphascustom = Create();
		assertNotNull(suphascustom);
		assertTrue("Barrier".equalsIgnoreCase(suphascustom.getCustomer().getName()));
		assertTrue("Compagnia dei Caraibi".equalsIgnoreCase(suphascustom.getSupplier().getName()));
	}

	@Test
	public void testUpdate() {
		SupplierHasCustomer suphascustom = Create();
		suphascustom = Update();
		assertEquals("Le Iris", suphascustom.getCustomer().getName());
		assertTrue("Velier".equalsIgnoreCase(suphascustom.getSupplier().getName()));
	}

	@Test
	public void testDelete() {
		SupplierHasCustomer suphascustom = Create();
		suphascustom = Delete();
		assertNull(suphascustom);
	}
	
	//Delete everything created during tests
	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		SupplierHasCustomer suphascustom = em.find(SupplierHasCustomer.class, 42);
		Customer customer = em.find(Customer.class, 21);
		Supplier supplier = em.find(Supplier.class, 22);
		if (suphascustom != null ) { // don't delete after Deletion test
			em.remove(suphascustom);
			em.remove(customer);
			em.remove(supplier);
		}
		em.getTransaction().commit();
	}

}
