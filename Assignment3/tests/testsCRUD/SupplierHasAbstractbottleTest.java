package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class SupplierHasAbstractbottleTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public SupplierHasAbstractbottle Create() {
		//Create Secondary entities
		em.getTransaction().begin();
		Abstractbottle abs_bottle = new Abstractbottle();
		abs_bottle.setId(21);
		abs_bottle.setAlc(41.0);
		abs_bottle.setCapacity(750);
		abs_bottle.setType("Mezcal");
		abs_bottle.setName("Nuestra Soledad");
		Supplier supplier = new Supplier();
		supplier.setName("Compagnia dei Caraibi");
		supplier.setId(22);
		em.persist(abs_bottle);
		em.persist(supplier);
		em.getTransaction().commit();
		// Create main entity
		SupplierHasAbstractbottle suphasbottle = new SupplierHasAbstractbottle();
		// Add to DB
		em.getTransaction().begin();
		suphasbottle.setId(42);
		suphasbottle.setAvailable(42);
		suphasbottle.setSupplier(supplier);
		suphasbottle.setAbstractbottle(abs_bottle);
		em.persist(suphasbottle);		
		em.getTransaction().commit();
		suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		return suphasbottle;
	}

	public SupplierHasAbstractbottle Update() {
		// Find Entity
		SupplierHasAbstractbottle suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		Supplier supplier = em.find(Supplier.class, 22);
		// Update
		em.getTransaction().begin();
		abs_bottle.setAlc(41.2);
		abs_bottle.setCapacity(700);
		abs_bottle.setName("Plymouth");
		abs_bottle.setType("Gin");
		supplier.setName("Velier");
		suphasbottle.setAbstractbottle(abs_bottle);
		suphasbottle.setSupplier(supplier);
		em.persist(abs_bottle);
		em.persist(supplier);
		em.getTransaction().commit();
		suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		return suphasbottle;
	}

	public SupplierHasAbstractbottle Delete() {
		// Find Entities
		SupplierHasAbstractbottle suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		Supplier supplier = em.find(Supplier.class, 22);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		em.getTransaction().begin();
		em.remove(suphasbottle);
		em.remove(supplier);
		em.remove(abs_bottle);
		em.getTransaction().commit();
		suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		return suphasbottle;
	}

	@Test
	public void testCreateRead() {
		SupplierHasAbstractbottle suphasbottle = Create();
		assertNotNull(suphasbottle);
		assertEquals(42, suphasbottle.getAvailable());
		assertEquals("Mezcal", suphasbottle.getAbstractbottle().getType());
		assertEquals("Nuestra Soledad", suphasbottle.getAbstractbottle().getName());
		assertEquals(750, suphasbottle.getAbstractbottle().getCapacity());
		assertEquals(41.0, suphasbottle.getAbstractbottle().getAlc(), 2);
		assertTrue("Compagnia dei Caraibi".equalsIgnoreCase(suphasbottle.getSupplier().getName()));
	}

	@Test
	public void testUpdate() {
		SupplierHasAbstractbottle suphasbottle = Create();
		suphasbottle = Update();
		assertEquals("Plymouth", suphasbottle.getAbstractbottle().getName());
		assertEquals(41.2, suphasbottle.getAbstractbottle().getAlc(), 2);
		assertEquals("Gin", suphasbottle.getAbstractbottle().getType());
		assertTrue("Velier".equalsIgnoreCase(suphasbottle.getSupplier().getName()));
	}

	@Test
	public void testDelete() {
		SupplierHasAbstractbottle suphasbottle = Create();
		suphasbottle = Delete();
		assertNull(suphasbottle);
	}
	
	//Delete everything created during tests
	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		SupplierHasAbstractbottle suphasbottle = em.find(SupplierHasAbstractbottle.class, 42);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		Supplier supplier = em.find(Supplier.class, 22);
		if (suphasbottle != null ) { // don't delete after Deletion test
			em.remove(suphasbottle);
			em.remove(abs_bottle);
			em.remove(supplier);
		}
		em.getTransaction().commit();
	}

}
