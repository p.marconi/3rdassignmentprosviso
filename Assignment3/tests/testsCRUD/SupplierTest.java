package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class SupplierTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Supplier Create() {
		// Create Entity
		Supplier supplier = new Supplier();
		supplier.setName("compagnia dei caraibi");
		supplier.setCompany("caraibi s.p.a");
		supplier.setId(20);
		// Add to DB
		em.getTransaction().begin();
		em.persist(supplier);
		em.getTransaction().commit();
		supplier = em.find(Supplier.class, 20);
		return supplier;
	}
	
	public Supplier createNullable() {
		// Create Entity
		Supplier supplier = new Supplier();
		supplier.setId(20);
		// Add to DB
		em.getTransaction().begin();
		em.persist(supplier);
		em.getTransaction().commit();
		supplier = em.find(Supplier.class, 20);
		return supplier;
	}

	public Supplier Update() {
		// Find Entity
		Supplier supplier = em.find(Supplier.class, 20);
		// Update
		em.getTransaction().begin();
		supplier.setName("Vecchio Magazzino Doganale");
		supplier.setCompany("Magazzino s.p.a");
		em.getTransaction().commit();
		supplier = em.find(Supplier.class, 20);
		return supplier;
	}

	public Supplier Delete() {
		// Find Entity
		Supplier supplier = em.find(Supplier.class, 20);
		em.getTransaction().begin();
		// Remove Entity
		em.remove(supplier);
		em.getTransaction().commit();
		supplier = em.find(Supplier.class, 20);
		// Test it
		return supplier;
	}

	@Test
	public void testCreateRead() {
		Supplier supplier = Create();
		assertNotNull(supplier);
		assertTrue("Compagnia dei Caraibi".equalsIgnoreCase(supplier.getName()));
		assertTrue("caraibi s.p.a".equalsIgnoreCase(supplier.getCompany()));
	}
	
	@Test
	public void testNullable() {
		Supplier supplier = createNullable();
		assertNotNull(supplier);
		assertEquals(null, supplier.getName());
		assertEquals(null, supplier.getCompany());
	}

	@Test
	public void testUpdate() {
		Supplier supplier = Create();
		supplier = Update();
		assertTrue("vecchio magazzino doganale".equalsIgnoreCase(supplier.getName()));
		assertTrue("magazzino s.p.a".equalsIgnoreCase(supplier.getCompany()));
	}

	@Test
	public void testDelete() {
		Supplier supplier = Create();
		supplier = Delete();
		assertNull(supplier);
	}

	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Supplier supplier = em.find(Supplier.class, 20);
		if (supplier != null) { // don't delete after Deletion test
			em.remove(supplier);
		}
		em.getTransaction().commit();
	}

}
