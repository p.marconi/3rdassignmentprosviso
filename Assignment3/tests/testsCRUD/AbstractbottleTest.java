package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Abstractbottle;

import org.junit.After;
import org.junit.Test;

public class AbstractbottleTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Abstractbottle Create() {
		// Create Entity
		Abstractbottle bottle = new Abstractbottle();
		bottle.setAlc(41.0);
		bottle.setCapacity(750);
		bottle.setName("Nuestra Soledad");
		bottle.setType("Mezcal");
		bottle.setId(23);
		// Add to DB
		em.getTransaction().begin();
		em.persist(bottle);
		em.getTransaction().commit();
		bottle = em.find(Abstractbottle.class, 23);
		return bottle;
	}

	public Abstractbottle Update() {
		// Find Entity
		Abstractbottle bottle = em.find(Abstractbottle.class, 23);
		// Update
		em.getTransaction().begin();
		bottle.setAlc(41.2);
		bottle.setCapacity(700);
		bottle.setName("Plymouth");
		bottle.setType("Gin");
		em.getTransaction().commit();
		bottle = em.find(Abstractbottle.class, 23);
		return bottle;
	}
	
	public Abstractbottle Nullable() {
		// Create Entity
		Abstractbottle bottle = new Abstractbottle();
		bottle.setType("Mezcal");
		bottle.setId(23);
		// Add to DB
		em.getTransaction().begin();
		em.persist(bottle);
		em.getTransaction().commit();
		bottle = em.find(Abstractbottle.class, 23);
		return bottle;
	}

	public Abstractbottle Delete() {
		// Find Entity
		Abstractbottle bottle = em.find(Abstractbottle.class, 23);
		em.getTransaction().begin();
		em.remove(bottle);
		em.getTransaction().commit();
		bottle = em.find(Abstractbottle.class, 23);
		return bottle;
	}

	@Test
	public void testCreateRead() {
		Abstractbottle bottle = Create();
		assertNotNull(bottle);
		assertEquals("Mezcal", bottle.getType());
		assertEquals("Nuestra Soledad", bottle.getName());
		assertEquals(750, bottle.getCapacity());
		assertEquals(41.0, bottle.getAlc(), 2);
	}

	@Test
	public void testUpdate() {
		Abstractbottle bottle = Create();
		bottle = Update();
		assertEquals("Gin", bottle.getType());
		assertEquals("Plymouth", bottle.getName());
		assertEquals(700, bottle.getCapacity());
		assertEquals(41.2, bottle.getAlc(), 2);
	}
	
	@Test
	public void testNullable() {
		Abstractbottle bottle = Nullable();
		assertNotNull(bottle);
		assertEquals(0.0, bottle.getAlc(), 2);
		assertEquals(0, bottle.getCapacity());
		assertNull(bottle.getName());
	}

	@Test
	public void testDelete() {
		Abstractbottle bottle = Create();
		bottle = Delete();
		assertNull(bottle);
	}

	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Abstractbottle bottle = em.find(Abstractbottle.class, 23);
		if (bottle != null) { // don't delete after Deletion test
			em.remove(bottle);
		}
		em.getTransaction().commit();
	}

}
