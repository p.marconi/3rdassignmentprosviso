package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Customer;

import org.junit.After;
import org.junit.Test;

public class CustomerTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Customer Create() {
		// Create Entity
		Customer suppliercustom = new Customer();
		suppliercustom.setName("Bobino");
		suppliercustom.setId(23);
		Customer customer = new Customer();
		customer.setName("Barrier");
		customer.setId(22);
		customer.setCustomer(suppliercustom);
		// Add to DB
		em.getTransaction().begin();
		em.persist(customer);
		em.persist(suppliercustom);
		em.getTransaction().commit();
		customer = em.find(Customer.class, 22);
		return customer;
	}
	
	public Customer createNullable() {
		// Create Entity
		Customer customer = new Customer();
		customer.setId(22);
		// Add to DB
		em.getTransaction().begin();
		em.persist(customer);
		em.getTransaction().commit();
		customer = em.find(Customer.class, 22);
		return customer;
	}

	public Customer Update() {
		// Find Entity
		Customer customer = em.find(Customer.class, 22);
		Customer suppliercustom = em.find(Customer.class, 23);
		// Update
		em.getTransaction().begin();
		suppliercustom.setName("Barrier");
		customer.setName("Le Iris");
		em.getTransaction().commit();
		customer = em.find(Customer.class, 22);
		return customer;
	}

	public Customer Delete() {
		// Find Entity
		Customer customer = em.find(Customer.class, 22);
		em.getTransaction().begin();
		em.remove(customer);
		em.getTransaction().commit();
		customer = em.find(Customer.class, 22);
		return customer;
	}

	@Test
	public void testCreateRead() {
		Customer customer = Create();
		assertNotNull(customer);
		assertTrue("Barrier".equalsIgnoreCase(customer.getName()));
		assertEquals(23, customer.getCustomer().getId());
		assertTrue("Bobino".equalsIgnoreCase(customer.getCustomer().getName()));
	}
	
	@Test
	public void testNullable() {
		Customer customer = createNullable();
		assertNotNull(customer);
		assertNull(customer.getName());
		assertNull(customer.getCustomer());
	}

	@Test
	public void testUpdate() {
		Customer customer = Create();
		customer = Update();
		assertTrue("Le Iris".equalsIgnoreCase(customer.getName()));
		assertTrue("Barrier".equalsIgnoreCase(customer.getCustomer().getName()));
	}

	@Test
	public void testDelete() {
		Customer customer = Create();
		customer = Delete();
		assertNull(customer);
	}

	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Customer customer = em.find(Customer.class, 22);
		Customer suppliercustom = em.find(Customer.class, 23);
		if (customer != null) { // don't delete after Deletion test
			em.remove(customer);
		}
		if(suppliercustom != null) {
			em.remove(suppliercustom);
		}
		em.getTransaction().commit();
	}

}
