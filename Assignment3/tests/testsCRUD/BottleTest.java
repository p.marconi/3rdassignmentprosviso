package testsCRUD;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.*;

import org.junit.After;
import org.junit.Test;

public class BottleTest {

	public final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Assignment3");
	public final EntityManager em = emf.createEntityManager();

	public Bottle Create() {
		//Create Secondary entities
		em.getTransaction().begin();
		Abstractbottle abs_bottle = new Abstractbottle();
		abs_bottle.setId(21);
		abs_bottle.setAlc(41.0);
		abs_bottle.setCapacity(750);
		abs_bottle.setType("Mezcal");
		abs_bottle.setName("Nuestra Soledad");
		Customer customer = new Customer();
		customer.setName("Barrier");
		customer.setId(22);
		em.persist(abs_bottle);
		em.persist(customer);
		em.getTransaction().commit();
		// Create main entity
		Bottle bottle = new Bottle();
		bottle.setId(23);
		bottle.setQuantity(4);
		bottle.setCustomer(customer);
		bottle.setAbstractbottle(abs_bottle);
		// Add to DB
		em.getTransaction().begin();
		em.persist(bottle);		
		em.getTransaction().commit();
		bottle = em.find(Bottle.class, 23);
		return bottle;
	}

	public Bottle Update() {
		// Find Entity
		Bottle bottle = em.find(Bottle.class, 23);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		Customer customer = em.find(Customer.class, 22);
		// Update
		em.getTransaction().begin();
		abs_bottle.setAlc(41.2);
		abs_bottle.setCapacity(700);
		abs_bottle.setName("Plymouth");
		abs_bottle.setType("Gin");
		customer.setName("Le Iris");
		bottle.setQuantity(80);
		bottle.setAbstractbottle(abs_bottle);
		bottle.setCustomer(customer);
		em.persist(abs_bottle);
		em.persist(customer);
		em.getTransaction().commit();
		bottle = em.find(Bottle.class, 23);
		return bottle;
	}

	public Bottle Delete() {
		// Find Entities
		Bottle bottle = em.find(Bottle.class, 23);
		Customer customer = em.find(Customer.class, 22);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		em.getTransaction().begin();
		em.remove(bottle);
		em.remove(customer);
		em.remove(abs_bottle);
		em.getTransaction().commit();
		bottle = em.find(Bottle.class, 23);
		return bottle;
	}

	@Test
	public void testCreateRead() {
		Bottle bottle = Create();
		assertNotNull(bottle);
		assertEquals("Mezcal", bottle.getAbstractbottle().getType());
		assertEquals("Nuestra Soledad", bottle.getAbstractbottle().getName());
		assertEquals(750, bottle.getAbstractbottle().getCapacity());
		assertEquals(41.0, bottle.getAbstractbottle().getAlc(), 2);
		assertEquals("Barrier", bottle.getCustomer().getName());
	}

	@Test
	public void testUpdate() {
		Bottle bottle = Create();
		bottle = Update();
		assertEquals(80, bottle.getQuantity());
		assertEquals("Le Iris", bottle.getCustomer().getName());
		assertEquals("Plymouth", bottle.getAbstractbottle().getName());
		assertEquals(41.2, bottle.getAbstractbottle().getAlc(), 2);
		assertEquals("Gin", bottle.getAbstractbottle().getType());
	}

	@Test
	public void testDelete() {
		Bottle bottle = Create();
		bottle = Delete();
		assertNull(bottle);
	}
	
	//Delete everything created during tests
	@After
	public void DeleteElement() {
		em.getTransaction().begin();
		Bottle bottle = em.find(Bottle.class, 23);
		Abstractbottle abs_bottle = em.find(Abstractbottle.class, 21);
		Customer customer = em.find(Customer.class, 22);
		if (bottle != null ) { // don't delete after Deletion test
			em.remove(bottle);
			em.remove(abs_bottle);
			em.remove(customer);
		}
		em.getTransaction().commit();
	}

}
