# 3rd Assignment ProSviSo

## Introduzione al Dominio

Il database è stato realizzato con lo scopo di replicare le interazioni all'interno del mondo dei bar.
Questa scelta è stata determinata da due fattori principali:
* Il mio grande interesse per l'ambiente applicativo ( e sfruttare le conoscenze acquisite durante le estati lavorative )
* La volontà di poter integrare il lavoro svolto con quello dell'[Assignment 2](https://elearning.unimib.it/pluginfile.php/525583/assignsubmission_file/submission_files/164318/Assignment%202_Paolo_Marconi_807172.pdf?forcedownload=1)

Per motivi di tempo ho deciso di tralasciare alcuni degli aspetti trattati nello scorso assignment, come la modellazione delle ricette o la parte di data retrival dai social network. Nel paragrafo successivo verrà analizzata l'intera struttura del database.

## La struttura del DB

![picture alt](https://i.imgur.com/SDwPEqv.png "Bars DB")

#### Le Tabelle

All'interno del database sono state create le seguenti tabelle:
* **Abstractbottle**, ossia la tipologia di bottiglia. Ho deciso di realizzare questo tipo di tabella poichè, per esperienza, la frequenza con cui le aziende apportano modifiche al proprio prodotto è elevata. In particolare si tende, a seconda della richiesta di mercato, a variare la percentuale alcolica del prodotto o il volume del recipiente. Apportando quindi una modifica alla tabella seguente, *le nuove istanze* risulteranno aggiornate coerentemente alle nuove *release*, mentre le istanze meno recenti manterranno le proprietà delle vecchie edizioni.
* **Bottle**, le vere e proprie bottiglie, istanze di *Abstractbottle*. Visto l'elevato numero di bottiglie che ogni bar possiede, ho deciso, per non appesantire il database e semplificare le transizioni, di inserire l'attributo **Quantity**. Questo comporta il fatto che ogni singola bottiglia **non** sia associata ad un proprio id, ma incrementi la quantità di un *gruppo di bottiglie* avente id unico.
* **Customer**, tabella che rappresenta il bar, avente un nome e un ulteriore bar negli attributi. Le informazioni riguardanti le relazioni tra tabelle verranno presentate nel prossimo paragrafo.
* **Supplier**, i fornitori dei bar, aventi un nome e una propria compagnia di riferimento (*Non Modellata*)

#### Le Relazioni


| Relazione                   |      Cardinalità      |  Update   | Delete |
|-----------------------------|:---------------------:|:----------|:--------|
| *Abstarctbottle → Bottle*   |  1:n                  | No Action |Cascade  |
| *Customer → Bottle*         |  1:n                  |   Cascade |Cascade  |
| *Customer → Customer*       |  1:1                  | No Action |Set Null |
| *Supplier → Abstractbottle* |  n:n                  | Cascade   |No Action|
| *Supplier → Customer*       |  n:n                  |   Cascade |No Action|


## I Test JUnit

Ciascun metodo `@test` è stato scritto pensando che ogni esecuzione potesse avvenire *singolarmente*.
Ho inoltre incluso un metodo con l'annotazione `@After` per cancellare le modifiche effettuate al db al termine di ogni caso di testing.
Sono stati presi come oggetto di test le proprietà **CRUD** di ogni *entity* e le relazioni nelle quali sono coinvolte, distinte rispettivamente dai package ***testCRUD*** e ***testRelationship***.
In questo modo, tutti i casi di test possono essere eseguiti con un singolo comando:
Click destro sul folder dei test → Run As → JUnit Test.


## Come Avviare il Progetto

1. Clonare il repository su eclipse
2. Lanciare il comando (all'intenro del folder `MySQL_Docker`)  docker-build dalla cmd, seguito dal comando docker-compose up. A questo punto il container con il database dovrebbe essere attivo.
3. Connettere il progetto di ecplipse al container docker attraverso la GUI "Data Source Explorer". I dati di connessione sono:
   * Database : bars
   * URL : jdbc:mysql://localhost:3306/bars
   * User name: paolo
   * Password: entro